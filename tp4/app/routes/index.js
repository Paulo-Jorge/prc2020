var express = require('express');
var router = express.Router();
var axios = require('axios')

var prefixos = `
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX noInferences: <http://www.ontotext.com/explicit>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
`

router.get('/', async(req, res, next) => {
  var getLink = "http://localhost:7200/repositories/" + "amd" + "?query="

  var query = `
    PREFIX amd: <http://prc.di.uminho.pt/2020/amd#>
    select ?titulo (count(?partitura) as ?numPartituras) ?identificador where {
        ?identificador rdf:type amd:Obra .
        ?identificador amd:temPartitura ?partitura .
        ?identificador amd:título ?titulo .
    }  group by ?titulo ?identificador
  `;

  listaObras = []
  axios.get(getLink + encodeURIComponent(prefixos + query))
    .then(dados => {
      dados.data.results.bindings.forEach(element => {
      
        identificador = element.identificador.value.split('http://prc.di.uminho.pt/2020/amd#')

        objeto = {titulo: element.titulo.value, numPartituras: element.numPartituras.value, identificador: identificador[1]}

        listaObras.push(objeto)
      })

      res.render('index', {listaObras: listaObras})
    })
    .catch(erro => console.log(erro))  
});

router.get('/:obraId', async(req, res, next) => {
  var obraId = req.params.obraId

  var getLink = "http://localhost:7200/repositories/" + "amd" + "?query="

  var query = `
    PREFIX amd: <http://prc.di.uminho.pt/2020/amd#>
    
    select ?partitura ?clave ?path ?afinacao ?voz{
        amd:` + obraId +  ` rdf:type amd:Obra .
        amd:` + obraId + ` amd:temPartitura ?partitura .
        
        optional {
            ?partitura amd:clave ?clave .
        }
        optional {
          ?partitura amd:afinação ?afinacao
        }
        optional {
          ?partitura amd:voz ?voz .    
        }
    
        ?partitura amd:path ?path .
    }
  `;

  listaPartituras = []
  axios.get(getLink + encodeURIComponent(prefixos + query))
    .then(dados => {
      dados.data.results.bindings.forEach(element => {
        objeto = {}

        path = "None"
        if(element.hasOwnProperty('path')){
          path = element.path.value
        }
        
        clave = "None"
        if(element.hasOwnProperty('clave')){
          clave = element.clave.value
        }

        afinacao = "None"
        if(element.hasOwnProperty('afinação')){
          afinacao = element.afinação.value
        }
        
        voz = "None"
        if(element.hasOwnProperty('voz')){
          voz = element.voz.value
        }

        identificadorPartitura = element.partitura.value.split('http://prc.di.uminho.pt/2020/amd#')

        objeto = {idPartitura: identificadorPartitura[1], path: path, voz: voz, afinacao: afinacao, clave: clave}

        listaPartituras.push(objeto)
      })

      res.render('obra', {idObra: obraId, listaPartituras: listaPartituras})
    })
    .catch(erro => console.log(erro))  
});

module.exports = router;
