#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import json
import requests

nomesEscritores = []

def readFile(file):
    with open(file) as f:
        data = json.load(f)

    nomeRealizador = ""
    sexo = ""

    i = 0
    while i < len(data):
        dicionario = {}
        dicionario.update(data[i])
        for valores in dicionario:
            if valores == 'nomeEscritor':
                for valor in dicionario[valores]:
                    if valor == 'value':
                        nomeEscritor = str(dicionario[valores][valor])
            if valores == 'sexo':
                for valor in dicionario[valores]:
                    if valor == 'value':
                        sexo = str(dicionario[valores][valor])

        nomeEscritor = re.sub(r'[,,/\"]', '', str(nomeEscritor))
        nomeEscritor = re.sub(r'[ \'.()–’]', '_', str(nomeEscritor))
       
        if(not(nomesEscritores.__contains__(nomeEscritor))):
            print("###  http://www.semanticweb.org/ontologies/2020/2/cinema#" + nomeEscritor)
            print(":" + nomeEscritor, "rdf:type owl:NamedIndividual ,")
            print("                :Escritor ,")
            print("                :Pessoa ;")
            if sexo == 'male':
                print("        :sexo \"M\" .")
            else:
                print("        :sexo \"F\" .")

            nomesEscritores.append(nomeEscritor)
    
        i = i + 1

readFile('../dados/escritores.json')
