var express = require('express');
var router = express.Router();
var axios = require('axios');

const appHost = require('../config/env').host;
const sparqlHost = "http://localhost:7200";

var prefixes = `
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX noInferences: <http://www.ontotext.com/explicit>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
`

function repos () {
    axios.get(sparqlHost + "/repositories")
    .then(dados => {
        let repos = []
        //repos = dados.data.results.bindings;
        for(let i = 0; i < dados.data.results.bindings.length; i++){
            repos[i] = dados.data.results.bindings[i].id.value
        }
    res.render('index', { repos });
    })
    .catch(erro => console.log(erro))
}

/* GET home page. */
router.get('/', function(req, res, next) {
	axios.get(sparqlHost + "/repositories")
	.then(dados => {
        let repos = []
		//repos = dados.data.results.bindings;
        for(let i = 0; i < dados.data.results.bindings.length; i++){
            repos[i] = dados.data.results.bindings[i].id.value
        }
    res.render('index', { repos });
	})
	.catch(erro => console.log(erro))
});


router.post('/',function(req, res){
  //var repo = req.repo;

  let query = req.query.query
  let repo = req.query.repoName
  console.log(repo)
  res.render('index', { repo });

  axios.get(sparqlHost + "/" + repo + "/namespaces")
    .then(dado => {
        console.log("tesssssssssssssssssssssssssssssssssssste")
      let prefixe = prefixes + 'PREFIX : <'+dado.data.results.bindings[0].namespace.value+'>'
      var encoded = encodeURIComponent(prefixe + query)
      queryURL = `http://localhost:7200/repositories/${repo}?query=${encoded}`

      axios.get(queryURL)
        .then(dados => {

            let repos = ['Teste'];

          /*dquery = []
          for(let i = 0; i < dados.data.results.bindings.length; i++)
            dquery[i] = dados.data.results.bindings[i]
          res.render('index', {dquery,data , title: 'SPARQL' });*/
          res.render('index', {repos:repos, resultado: dados.data });
        })
        .catch(erro => {
          erro => console.log(erro);
        })
    })
    .catch(erro => {
    erro => console.log(erro);
    })
})

module.exports = router;
