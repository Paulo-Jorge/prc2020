var axios = require('axios')
const myhost = require('../config/database').host
const urlGraphDB = require('../config/database').onthology
const prefixes = require('../config/database').prefixes

exports.execQuery = async function(method, query){
    var getLink = urlGraphDB + "?query="
    var postLink = urlGraphDB + "/statements"
    var encoded = encodeURIComponent(prefixes + query)
    var response
    try{
        switch(method) {
            case "query":
                response = await axios.get(getLink + encoded)
                break;
            case "update":
                response = await axios.post(postLink, `update=${encoded}`)
                break;
            default:
                response = await axios.get(getLink + encoded)
                break;
        }
        return response.data
    }catch(error){
        throw(error)
    }
}

exports.normalize = function(response) {
    return response.results.bindings.map(obj =>
        Object.entries(obj)
            .reduce((new_obj, [k,v]) => (new_obj[k] = v.value, new_obj),
                    new Object()));
};