var express = require('express');
var router = express.Router();
var movies = require('../controllers/movies.js')

router.get('/', async (req, res, next) => {
	try{
        res.jsonp(await movies.listar(req.query))
    } catch(erro){
        res.status(500).send(`Erro: ${erro}`)
    }
})

router.get('/:id', async (req, res, next) => {
	try{
        var filme = await movies.findOne(req.params.id);
        filme.produtores = await movies.getProducers(req.params.id);
        filme.generos = await movies.getGeneros(req.params.id);
        filme.personagens = await movies.getPersonagens(req.params.id);
        filme.realizadores = await movies.getRealizadores(req.params.id);
        res.jsonp(filme)
    } catch(erro){
        res.status(500).send(`Erro: ${erro}`)
    }
})

module.exports = router;
