var express = require('express');
var router = express.Router();
var movies = require('../controllers/movies.js')

router.get('/', async (req, res, next) => {
	try{
        res.jsonp(await movies.listarAtores(req.query))
    } catch(erro){
        res.status(500).send(`Erro: ${erro}`)
    }
})

router.get('/:id', async (req, res, next) => {
	try{
        res.jsonp(await movies.AtoresfindOne(req.params.id))
    } catch(erro){
        res.status(500).send(`Erro: ${erro}`)
    }
})

module.exports = router;
